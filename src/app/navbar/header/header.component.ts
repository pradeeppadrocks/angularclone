import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../core/services/session/session.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private _sessionService:SessionService,
    private _router: Router,
    ) { }

  ngOnInit() {
  }

  Logout(){
    if(this._sessionService.removeSession()){
      alert('Logout Successfully')
      this._router.navigateByUrl('/login');

    }else{
      alert('something wrong')
    }
  }
}
