import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppinessRoutingModule } from './appiness-routing.module';
import { NavbarModule } from 'src/app/navbar/navbar.module';
import { AppinessService } from './service/appiness.service';
import { MaterialModule } from 'src/app/material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductComponent } from './product/product.component';

@NgModule({
  declarations: [ProductComponent],
  imports: [
    CommonModule,
    AppinessRoutingModule,
    NavbarModule,
    SharedModule,
    MaterialModule
  ],
  providers:[AppinessService]
})
export class AppinessModule { }
