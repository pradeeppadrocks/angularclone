import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppinessService {
  private url = "http://localhost:3000/category/"
  private url2 = "http://localhost:3000/products/"

  private url3 = "http://localhost:3000/products/"


  constructor(private _http: HttpClient) { }

  getAllProducts(data) {
    return this._http.post(`${this.url3}list`, data);
  }


  deleteProducts(id: number) {
    return this._http.delete(`${this.url3}list/${id}`);
  }


  saveCategory(data) {
    return this._http.post(`${this.url}create`, data);
  }

  updateCategory(data) {
    return this._http.patch(`${this.url3}update`, data);
  }

  addProduct(data) {
    return this._http.post(`${this.url2}create`, data);
  }

}
