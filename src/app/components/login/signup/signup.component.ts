import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  loginForm: FormGroup
  loginSubscription$: Subscription
  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _loginService: LoginService,
  ) { }

  ngOnInit() {
    this.loginForm = this._fb.group({
      email: ['', [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]],
      password: [null, [Validators.required]],
    })
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.loginSubscription$ = this._loginService.signup(this.loginForm.value).subscribe((res) => {
        if (res.code == 201) {
          alert('Registered succesfully now you are able to login')
        } else if (res.code == 409) {
          alert('email already exists')
        }
      }, (err: any) => {
      })
    } else {
      const keysSignIn = Object.keys(this.loginForm.value);
      keysSignIn.forEach(val => {
        const ctrl = this.loginForm.controls[val];
        if (!ctrl.valid) {
          ctrl.markAsTouched();
        };
      });
    }
  }
}
