import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DashboardService } from '../services/dashboard.service';
import { Subscription, Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  dtOptions: DataTables.Settings = {};
  categoryListSubscription$: Subscription;
  persons: any;
  pageIndex = 0;
  pageLength = 10;

  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  categoryEditName: any;
  categoryyID: any;
  updateBool = false;
  categoryProductID: any;
  productBool = false;
  productName = '';
  productPrice = '';
  message: any;
  catName='';



  constructor(
    // tslint:disable-next-line: variable-name
    private _dashboardService: DashboardService,
    // tslint:disable-next-line: variable-name
    private _Router: Router,

  ) { }

  ngAfterViewInit() {
    this.dtTrigger.next();
    this.afterInit();
  }

  saveCategory() {
    const category = {
      categoryName: this.catName
    };
    this._dashboardService.saveCategory(category).subscribe((res: any) => {
      this.message = res.message;

      setTimeout(() => {
        this.message = '';

      }, 4000);
      this.pageIndex = 0;
      this.pageLength = 10;
      this.rerenderGrid();

    }, (err: any) => {
      alert('failed');

    });
  }

  editCate(cateId, cateName) {
    this.updateBool = true;
    this.categoryEditName = cateName;
    this.categoryyID = cateId;

  }

  EditCategory(categoryName, cateId) {
    const category = {
      id: this.categoryyID,
      // tslint:disable-next-line: object-literal-shorthand
      categoryName: categoryName
    }
    this._dashboardService.updateCategory(category).subscribe((res) => {
      alert('successfully created');
      this.updateBool = false;

      this.pageIndex = 0;
      this.pageLength = 10;
      this.rerenderGrid();

    }, (err: any) => {
      alert('failed');

    });

  }
  afterInit() {
    // This is to get table instance
    // Get event when page number is changed
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.on('page.dt', () => {
        const pageInfo = dtInstance.page.info();
        dtInstance.page.len();
        this.pageIndex = pageInfo.page;
        this.pageLength = pageInfo.length;
      });
    });

    // Get event when page length is changed
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.on('length.dt', (e, settings, len) => {
        const pageInfo = dtInstance.page.info();
        this.pageLength = len;
        this.pageIndex = pageInfo.page;
      });
    });
  }

  ngOnInit(): void {


    this.gridBinding();


  }
  gridBinding() {
    const that = this;

    this.dtOptions = {
      pagingType: 'full_numbers',
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        dataTablesParameters.draw = this.pageIndex + 1;
        dataTablesParameters.length = this.pageLength;
        const temdata = {
          pageNo: this.pageIndex + 1,
          pageSize: this.pageLength,
        }
        this._dashboardService.getAllCategory(temdata).subscribe(resp => {

          this.persons = resp[0].category;


          if (this.persons.length >= 1) {

            console.log("Legth:", this.persons.length)


            this.afterInit();

            callback({
              recordsTotal: resp[0].totalCount[0].count,
              recordsFiltered: resp[0].totalCount[0].count,
              data: []
            });
          } else {
            that.persons = [];
            callback({
              recordsTotal: 0,
              recordsFiltered: 0,
              data: []
            });
          }


        }, (err: any) => {
          this._Router.navigate(['/login']);

        });
      },
      columns: [{ data: 'index', orderable: false }, { data: 'firstName' }, { data: 'edit' }, { data: 'delete' }, { data: 'add category' }]
    };

  }

  addProduct(cateProductID) {
    this.categoryProductID = cateProductID;
    this.productBool = true;
  }

  addNewProduct() {
    const productData = {
      name: this.productName,
      price: this.productPrice,
      categoryId: this.categoryProductID

    }

    console.log('.......:', productData);
    this._dashboardService.addProduct(productData).subscribe((res: any) => {
      alert('product is created for this category');
      // this.gridBinding();
      this.productBool = false;
      this.pageIndex = 0;
      this.pageLength = 10;
      this.rerenderGrid();

    }, (err: any) => {
    })

  }

  deleteById(id) {
    this._dashboardService.deleteCategoryOfProducts(id).subscribe((res: any) => {
      alert('Its associated product also deleted')
      // this.gridBinding();
      // this.ngOnInit()
      this.pageIndex = 0;
      this.pageLength = 10;
      this.rerenderGrid();
    }, (err: any) => {
    })
  }
  rerenderGrid(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}
