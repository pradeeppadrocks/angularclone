import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from 'src/app/core/services/http/http.service';

@Injectable()
export class DashboardService {

  private url = "http://localhost:3000/category/"
  private url2 = "http://localhost:3000/products/"

  constructor(private _http: HttpClient) { }
  getAllCategory(data) {
    return this._http.post(`${this.url}list`, data);
  }


  deleteCategoryOfProducts(id: number) {
    return this._http.delete(`${this.url}list/${id}`);
  }
  saveCategory(data) {
    return this._http.post(`${this.url}create`, data);
  }

  updateCategory(data) {
    return this._http.patch(`${this.url}update`, data);
  }

  addProduct(data) {
    return this._http.post(`${this.url2}create`, data);
  }

}
