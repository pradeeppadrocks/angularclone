import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

  public keyName: string = 'pradeep_shetty';
  private tokenName: string = 'access_token';

  private role: string = 'role';

  constructor() { }

  /**
  * Set session item
  * @param key, item
  * @returns boolean
  */
  setSession(item: any, key: string = this.keyName) {
    localStorage.setItem(key, JSON.stringify(item));
    let insertItem = this.getSession(key);
    if (insertItem) {
      return true;
    }
  }

  /**
  * Get session item
  * @param key
  * @returns Object
  */
  getSession(key: string = this.keyName) {
    let item = JSON.parse(localStorage.getItem(key));
    return item;
  }


  /**
  * Remove session item
  * @param key
  * @returns boolean
  */
  removeSession(key: string = this.keyName) {
    localStorage.removeItem(key);
    let insertItem = this.getSession(key);
    if (!insertItem) {
      return true;
    }
  }

  /**
  * Clear session
  * @returns boolean
  */
  clearSession() {
    localStorage.clear();
    return true;
  }

  /**
  * Get token
  * @param key, tokenName
  * @returns string
  */
  getToken(key: string = this.keyName, tokenName: string = this.tokenName) {
    let item = JSON.parse(localStorage.getItem(key));
    if (item && item.hasOwnProperty(tokenName)) {
      return item[tokenName];
    }
    return null;
  }



  getRole(key: string = this.keyName, roleName: string = this.role) {
    let item = JSON.parse(localStorage.getItem(key));
    if (item && item.hasOwnProperty(roleName)) {
      return item[roleName];
    }
    return null;
  }

  /**
  * Check login status
  * @param key, tokenName
  * @returns boolean
  */
  checkLogin(key: string = this.keyName, tokenName: string = this.tokenName) {
    // return !!this.getToken(key, tokenName) != null
    if (this.getToken(key, tokenName) != null) {
      return true;
    }

    return false;
  }


}
